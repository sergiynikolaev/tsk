﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections.ObjectModel;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using Microsoft.Research.DynamicDataDisplay;
using TSKNet.TSKModule.MembershipFunctions;
using TSKNet.TSKModule;
using TSKNet.TSKModule.ParametricIdentification;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Win32;
using GUI.Components.CSV;
using TSKNet.TSKModule.Ranges;

namespace GUI
{
    /// <summary>
    /// Here all logic for GUI MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region  Init
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }
        #endregion

        private TSK<FastBellFunc> net;
        private double[][] X;
        private double[] Y;

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            base.OnInitialized(e);
            InitGraphics();
        }

        #region Graphics

        EnumerableDataSource<double> xDataSource;
        //      CompositeDataSource compositeDataSource1;
        private void InitGraphics()
        {
            //             compositeDataSource1 = new CompositeDataSource();

        }

        private void VisualizeY(double[] y)
        {
            xDataSource = new EnumerableDataSource<double>(GenerateX(y));
            xDataSource.SetXMapping(xi => xi/*dateAxis.ConvertToDouble(x)*/);
            //plotter. Remove UserElements();
            DrawY(xDataSource, y);
            plotter.Viewport.FitToView();

        }

        private static double[] GenerateX(double[] y)
        {
            double[] x = new double[y.Length];
            for (int i = 0; i < y.Length; i++)
                x[y.Length-i-1] = i;
            return x;
        }

        private void VisualizeAll(double[] y, double[] sim)
        {

            var xDataSource = new EnumerableDataSource<double>(GenerateX(y));
            xDataSource.SetXMapping(xi => xi/*dateAxis.ConvertToDouble(x)*/);
            DrawSimulation(xDataSource, sim);
            DrawY(xDataSource, y);
            plotter.Viewport.FitToView();

        }

        private void DrawY(EnumerableDataSource<double> xDataSource, double[] y)
        {
            var numberOpenDataSource = new EnumerableDataSource<double>(y);
            numberOpenDataSource.SetYMapping(yi => yi);
            CompositeDataSource compositeDataSource1 = new CompositeDataSource(xDataSource, numberOpenDataSource);
            //compositeDataSource1.(/*xDataSource, */numberOpenDataSource);
            plotter.AddLineGraph(compositeDataSource1,
              new Pen(Brushes.Blue, 2),
              new CirclePointMarker { Size = 10.0, Fill = Brushes.Red },
              new PenDescription("Real Data"));
        }

        private void DrawSimulation(EnumerableDataSource<double> xDataSource, double[] sim)
        {
            var numberClosedDataSource = new EnumerableDataSource<double>(sim);
            numberClosedDataSource.SetYMapping(y => y);
            CompositeDataSource compositeDataSource2 = new CompositeDataSource(xDataSource, numberClosedDataSource);
            plotter.AddLineGraph(compositeDataSource2, new Pen(Brushes.Green, 2),
                new TrianglePointMarker
                {
                    Size = 10.0,
                    Pen = new Pen(Brushes.Black, 2.0),
                    Fill = Brushes.GreenYellow
                },
                new PenDescription("Predicted"));
        }

        #endregion

        #region Load and Save Net
        #region Loading
        private void LoadNetBtn_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TSK<FastBellFunc>));
            serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
            serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);

            OpenFileDialog ofd = FileDialogInit();
            if (ofd.ShowDialog() == true)
            {
                FileWasPickedUserNotification(ofd.FileName);

                FileStream fs = new FileStream(ofd.FileName, FileMode.Open);
                try
                {
                    net = (TSK<FastBellFunc>)serializer.Deserialize(fs);
                }
                catch
                {
                    WrongFormatUserNotification(); return;
                }
                fs.Close();

                ModifyNetViewSizeParameters();
                MessageBox.Show("Loading complete.");
            }
            else
                FileWasNotPickedUserNotification();
        }

        private static OpenFileDialog FileDialogInit()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "XML Files(*.xml)|*.xml";
            return ofd;
        }

        private void FileWasPickedUserNotification(string fn)
        {
            SystemMonitorTB.AppendText("File " + fn + " was picked successfully\n");
        }

        private void FileWasNotPickedUserNotification()
        {
            SystemMonitorTB.AppendText("File was not picked!!!\n");
        }

        private void ModifyNetViewSizeParameters()
        {
            RulesNumber.Text = net.RulesNumber.ToString();
            RuleLength.Text = net.RuleLength.ToString();
        }

        public void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            SystemMonitorTB.AppendText("ParsingNet::Unknown Node:" + e.Name + "\t" + e.Text + "\n");
        }

        public void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            SystemMonitorTB.AppendText("ParsingNet::Unknown attribute " + attr.Name + "='" + attr.Value + "'\n");
        }
        #endregion
        #region Saving

        private void SaveNet_Click(object sender, RoutedEventArgs e)
        {

            SaveFileDialog ofd = new SaveFileDialog();
            ofd.Filter = "XML File(*.xml)|*.xml";
            if (ofd.ShowDialog() == true)
            {
                FileWasPickedUserNotification(ofd.FileName);
                TextWriter writer = new StreamWriter(ofd.FileName);
                XmlSerializer serializer = new XmlSerializer(typeof(TSK<FastBellFunc>));
                serializer.Serialize(writer, net);
                writer.Close();
                ModifyNetViewSizeParameters();
                MessageBox.Show("Saving complete.");
            }
            else
                FileWasNotPickedUserNotification();
        }

        #endregion
        #endregion

        #region System monitor
        private void ClearSysremMonitorBtn_Click(object sender, RoutedEventArgs e)
        {
            SystemMonitorTB.Clear();
        }

        #endregion

        #region Net training
        Trainer<TSK<FastBellFunc>> tr;

        private void StartTrainingBtn_Click(object sender, RoutedEventArgs e)
        {
            if ((X == null) || (X.Length == 0)) { MessageBox.Show("Please load dataset first!!! "); return; }
            if (net == null) { MessageBox.Show("Please init net first!"); return; }
            if (X[0].Length != net.RuleLength) { MessageBox.Show("Data columns do not match net rule length!"); return; }
            //Init ranges here

            tr = new Trainer<TSK<FastBellFunc>>(net);
            tr.GradLogEvent += GradLog;
            tr.IterationEndLogEvent += Log;
            int cyc = 0;
            try
            {
                tr.H0 = Double.Parse(InitialH0.Text);
                tr.Growth = Double.Parse(hGrowth.Text);
                tr.Fade = Double.Parse(hFade.Text);
                tr.dMSE_Eps = Double.Parse(dmse_Eps.Text);
                tr.hEps = Double.Parse(hEps.Text);
                tr.MaxIterationsCount = Int32.Parse(MaxCyclesPerIter.Text);
                cyc = Int32.Parse(MaxIterCnt.Text);
            }
            catch
            {
                MessageBox.Show("Unknown format... \nPlease check training parameters!!!"); return;
            }

            //ANFIS Mode
            if (IsTSKNet.IsChecked == false) 
                tr.ANFISMode = true;
            else
                tr.ANFISMode = false;

            net = tr.Train(X, Y, cyc);
            SystemMonitorTB.AppendText("Training Finnished successfully...\nNetwork is:" + net + "Drawing results...\n");
            SimulateNetAndShowResults();
        }

        private void SimulateNetAndShowResults()
        {
            double[] sim = new double[Y.Length];
            for (int i = 0; i < Y.Length; i++)
                sim[i] = net.Simulate(X[i]).Val;

            VisualizeAll(Y, sim);
            CalculateAndShowFinalErrors();
        }

        public void GradLog(int rule, int conj, int param, int cnt, double h0, double prevMseValTest, double prevMseValTrain)
        {
            if (IsGradLogOn.IsChecked == true)
                SystemMonitorTB.AppendText("ijk=" + rule + " " + conj + " " + param + "   cnt=" + cnt + "\th0=" + h0 + "\tprevVal=" + prevMseValTest + "\n");
        }

        private void Log(Trainer<TSK<FastBellFunc>> tr, double MSEtrainCurrent, double MSEtrainPrev, double MSEtestCurrent, double MSEtestPrev, double h0, int cnt)
        {
            if (IsEventLogOn.IsChecked == true)
            {
                if (Math.Abs(h0) <= tr.hEps) SystemMonitorTB.AppendText("Stop by h0==(" + h0 + ") <= hEps\n");
                if (cnt >= tr.MaxIterationsCount) SystemMonitorTB.AppendText("Stop by max iter cnt==(" + cnt + ")\n");
                if (Math.Abs(MSEtrainCurrent - MSEtrainPrev) <= tr.dMSE_Eps) SystemMonitorTB.AppendText("Stop by  Math.Abs(MeanSqrError(X, Y)-prevVal)==(" + Math.Abs(MSEtrainCurrent - MSEtrainPrev) + ") <=eps\n");
            }
        }

        private void InitNewNetBtn_Click(object sender, RoutedEventArgs e)
        {
            /* InputRanges ir = new InputRanges(3, BellFunc.pCount);

             ir[0] = new WRanges(FastBellFunc.pCount);
             ir[0].R[0] = new Range(-10, 10);
             ir[0].R[1] = new Range(0.001, 5);

             ir[1] = new WRanges(FastBellFunc.pCount);
             ir[1].R[0] = new Range(20, 30);
             ir[1].R[1] = new Range(30, 40);

             ir[2] = new WRanges(FastBellFunc.pCount);
             ir[2].R[0] = new Range(40, 50);
             ir[2].R[1] = new Range(50, 60);
             */

            try
            {
                net = new TSK<FastBellFunc>(Int32.Parse(RulesNumber.Text), Int32.Parse(RuleLength.Text));
            }
            catch
            {
                /*SystemMonitorTB.AppendText()*/
                MessageBox.Show("Please enter correct Net Parameteres!!");
            }

        }


        #endregion

        #region Load Save Data

        private void LoadDataBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text file with ';' delimiters(*.csv)|*.csv";
            List<List<double>> inArr = new List<List<double>>();

            if (ofd.ShowDialog() == true)
            {
                FileWasPickedUserNotification(ofd.FileName);
                try
                {
                    inArr = ParseCSV(ofd);
                }
                catch
                {
                    WrongFormatUserNotification(); return;
                }
               // MessageBox.Show("Loading complete.");
            }
            else
                FileWasNotPickedUserNotification();
            if (ofd.FileName == "") return;
            //OutputInputArray(inArr);
            FormGridView(inArr);
            FormXY(inArr);
            VisualizeY(Y);
        }

        private void OutputInputArray(List<List<double>> inArr)
        {
            foreach (var row in inArr)
            {
                foreach (var s in row)
                    SystemMonitorTB.AppendText(s + " ");
                SystemMonitorTB.AppendText("\n");
            }
        }

        private void FormGridView(List<List<double>> inArr)
        {
            var ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);

            for (int cnames = 0; cnames < inArr[0].Count; cnames++)
                if (cnames + 1 == inArr[0].Count)
                    dt.Columns.Add("y");
                else
                    dt.Columns.Add("x" + cnames);

            for (int row = 0; row < inArr.Count; row++)
            {
                DataRow newRow = dt.NewRow();
                List<object> values = new List<object>();
                for (int col = 0; col < inArr[row].Count; col++) values.Add(inArr[row][col]);
                newRow.ItemArray = values.ToArray();
                dt.Rows.Add(newRow);
            }

            ViewDataGrid.ItemsSource = ds.Tables[0].DefaultView;

        }

        private void FormXY(List<List<double>> inArr)
        {
            int xCnt = inArr[0].Count - 1;
            X = new double[inArr.Count][];
            Y = new double[inArr.Count];

            for (int row = 0; row < inArr.Count; row++)
            {
                X[row] = new double[xCnt];
                for (int col = 0; col < inArr[row].Count; col++)
                    if (col < xCnt)
                        X[row][col] = inArr[row][col];
                    else
                        Y[row] = inArr[row][col];
            }

            //OutputXY();

        }

        private void OutputXY()
        {

            for (int row = 0; row < X.Length; row++)
            {
                for (int col = 0; col < X[row].Length; col++)
                    SystemMonitorTB.AppendText(X[row][col] + " ");
                SystemMonitorTB.AppendText("\n");
                SystemMonitorTB.AppendText(Y[row] + " \n");
            }
        }

        private List<List<double>> ParseCSV(OpenFileDialog ofd)
        {
            List<List<double>> inArr = new List<List<double>>();
            using (CsvFileReader reader = new CsvFileReader(ofd.FileName))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    List<double> sList = new List<double>();
                    foreach (string s in row)
                    {
                        SystemMonitorTB.AppendText(s + " ");
                        sList.Add(Double.Parse(s));
                    }
                    SystemMonitorTB.AppendText("\n");
                    inArr.Add(sList);
                }
            }
            return inArr;
        }

        private void WrongFormatUserNotification()
        {
            SystemMonitorTB.AppendText("Can not Parse given file: Wrong format!\n");
        }

        private void SaveDataBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.Filter = "Text file with ';' delimiters(*.csv)|*.csv";

            if (ofd.ShowDialog() == true)
            {
                FileWasPickedUserNotification(ofd.FileName);
                //
                SystemMonitorTB.AppendText("Sorry, This functionality is anavailable for now\n");
                //
                MessageBox.Show("Saving complete.");
            }
            else
                FileWasNotPickedUserNotification();
        }

        #endregion

        private void SimulateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (X == null) MessageBox.Show("Please Load Data First");
            if (net == null) MessageBox.Show("Please Load Network First");
            SimulateNetAndShowResults();
           
        }

        private void CalculateAndShowFinalErrors()
        {
            if(net == null)return;
            if (tr == null) tr = new Trainer<TSK<FastBellFunc>>(net);
            string resMsg="Final criteria are:\nMSE = "+ tr.MeanSqrError(X,Y)+"\nMAE = "+tr.MAE(X,Y)+"\nMAPE = "+tr.MAPE(X,Y)+"\n";
            MessageBox.Show(resMsg);
            SystemMonitorTB.AppendText(resMsg);

        }


    }
}
