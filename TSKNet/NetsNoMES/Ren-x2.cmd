@echo off
rem ~n - name only
rem ~x extension only

rem Forming File List
@echo #> FileList.log
for /F "usebackq delims==" %%i in ( `dir /b`) do (
if /I %%~xi == .xml  echo %%~ni>>FileList.log
)
rem Parsing File list 

rem Readinf mode: Line by line(F/)
rem Comment is ";"  delims=","
rem Strusture of reading doc is: command , arg1, argrest
set %%I = %%i

FOR /F "eol=# tokens=1,2* delims=) usebackq" %%l in (FileList.log) do (

echo "old&new:" "%%l)%%m.xml" "%%m.xml"
rename "%%l)%%m.xml" "%%l).xml"
)

@echo Files Changed
type FileList.log

 @pause Finnished Successfully
