﻿using System;
using TSKNet.TSKModule;
using TSKNet.TSKModule.Ranges;
using TSKNet.TSKModule.MembershipFunctions;


using LinearAlgebra.Matricies;
using System.Globalization;
using System.Timers;
using System.Diagnostics;
using TSKNet.TSKModule.ParametricIdentification;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using GUI.Components.CSV;

namespace TSKNet
{
    class Program
    {


        static void Main(string[] args)
        {
            ///Here you will find some examples - to activate - uncomment
            /// Basic example
            //CreateAndTrainFNN();


            /// Optimal Trainer Example
            CreateSimpleANFISNetAndUseTrainExpertExample();

            /// FNN Optimal Parameter identification Example
            //FindOptimalNetConfigExample();

        }

        #region EXAMPLES
        public static void CreateAndTrainFNN()
        {
            int featuresNumber = 2;     //what is the input data dimensionality
            int rulesNumber = 9;        //what is the net capasity

            TSK<FastBellFunc> net = new TSK<FastBellFunc>(rulesNumber, featuresNumber);
            
            ///This is our train dataset =)
            double[][] X = new double[][]{
                 /*   new[]{ 0.404, 0.094, 0.044}, 
                    new[]{ 0.071, 0.802, 0.559}, 
                    new[]{ 0.685, 0.906, 0.977}, 
                    new[]{ 0.991, 0.556, 0.287}, 
                    new[]{ 0.227, 0.528, 0.116}};*/
                 new[]{0.0,0.0},
                 new[]{0.0,0.5},
                 new[]{0.0,1.0},
                 new[]{0.5,1.0},
                 new[]{0.5,0.5},
                 new[]{0.5,0.0},
                 new[]{1.0,0.0},
                 new[]{1.0,0.5},
                 new[]{1.0,1.0}
            };
            double[] Y = new double[] { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 };
            /// Create trainer
            Trainer<TSK<FastBellFunc>> tr = new Trainer<TSK<FastBellFunc>>(net);
            tr.ANFISMode = true;
            /// Uncomment Events to see detailed process of learning
             tr.GradLogEvent += GradLog;
            // tr.IterationEndLogEvent += Log;
            /// Set network training parametes
            tr.H0 = 0.05;
            tr.Growth = 0.8;
            tr.Fade = 1.2;
            tr.MaxIterationsCount = 150;
            Console.WriteLine("Training the network...");

            int maxTrainIterations = 10; 
            net = tr.Train(X, Y, maxTrainIterations);

            Console.WriteLine(net.ToString());
            ///Test on train set
            Console.WriteLine("Test phase:");
            Console.WriteLine("Test on 0.1; net.sim = "+net.Simulate(new double[] { 0.0, 0.0}).ToString());
            Console.WriteLine("Test on 0.2; net.sim = " + net.Simulate(new double[] { 0.0, 0.5 }).ToString());
            Console.WriteLine("Test on 0.3; net.sim = " + net.Simulate(new double[] { 0.0, 1.0 }).ToString());
            Console.WriteLine("Test on 0.5; net.sim = " + net.Simulate(new double[] { 0.5, 0.5 }).ToString());
            Console.WriteLine("Test on 0.9; net.sim = " + net.Simulate(new double[] { 1.0, 1.0 }).ToString());
            PAKTE();
        }

        /// <summary>
        /// Create new FNN with 5 rules for 3 dimensional input for regression task
        /// </summary>
        public static void CreateSimpleANFISNetAndUseTrainExpertExample()
        {
            int featuresNumber = 3;     //what is the input data dimensionality (2..4 datasets currently available in .\data folder)
            int rulesNumber = 5;        //what is the net capasity
            int reinitRoundsNumber = 3; //how many times training should be restarted
            TSK<FastBellFunc> net = new TSK<FastBellFunc>(rulesNumber, featuresNumber);

            ///Serialisation to save network parameters
            XmlSerializer serializer = new XmlSerializer(typeof(TSK<FastBellFunc>));

            ///Create Trainer
            OptimalNetExpert<FastBellFunc> netFinder = new OptimalNetExpert<FastBellFunc>();
            netFinder.ANFISMode = true; 
            double[][] X;
            double[] Y;

            using (StreamWriter w = File.AppendText("logANFIS.txt"))
            {
                string fn = "..\\..\\..\\data\\Input50n x" + featuresNumber + ".csv";
                Console.WriteLine("Loading data from " + fn);
                LoadData(fn, out  X, out Y);
                Console.WriteLine("Data loaded successfully\nTraining started ...");

                ///Here network training goes
                netFinder.Reset();
                net = netFinder.FindOptimalSolution(X, Y, rulesNumber, reinitRoundsNumber);


                ///Save trained model
                TextWriter writer = new StreamWriter(((netFinder.ANFISMode) ? 'a' : 'z') + "Net r" + rulesNumber + "x" + X[0].Length + "set(" + X.Length + ") mse" + netFinder.CurrentOptimalNet.CurrentMinErr + ".xml");

                serializer.Serialize(writer, net);
                writer.Close();

                Console.WriteLine("Training finished - mean squared error is " + netFinder.CurrentOptimalNet.CurrentMinErr);
            }

            PAKTE();
        }


        public static void FindOptimalNetConfigExample()
        {

            ///Serialisation to save network parameters
            XmlSerializer serializer = new XmlSerializer(typeof(TSK<FastBellFunc>));
            /// Finds optimal net inputs and rules
            CalculateAllNetsThroughAllDatasets(serializer);
            /// Calculates Criteria over all nets
            CalculateMAPE_MAE_MSE_ThroughAllNetsAndAllDatasets(serializer);
            PAKTE();
        }


        /// <summary>
        /// This is example parametric optimal net search
        /// </summary>
        /// <param name="serializer"></param>
        private static void CalculateAllNetsThroughAllDatasets(XmlSerializer serializer)
        {
            Console.WriteLine("Identifying best parameters for FNN");
            OptimalNetExpert<FastBellFunc> netFinder = new OptimalNetExpert<FastBellFunc>();
            netFinder.ANFISMode = true;
            double[][] X;
            double[] Y;

            /// Search for optimal features number (starting from 2 features to 4) - all datasets are being loaded from .\data folder
            for (int featuresN = 2; featuresN <= 4; featuresN++)
            {
                using (StreamWriter w = File.AppendText("logANFIS.txt"))
                {
                    Console.WriteLine(" Loading dataset with " + featuresN + " features...");
                    string fileName = "..\\..\\..\\data\\Input50n x" + featuresN + ".csv";
                    LoadData(fileName, out  X, out Y);

                    Log("dataFile= " + fileName, w);

                    TSK<FastBellFunc> net = new TSK<FastBellFunc>();

                    /// Search for optimal rules number in fuzzy network
                    for (int rulesNumber = 5; rulesNumber < 12; rulesNumber++)
                    {

                        Console.WriteLine("   Training net with " + rulesNumber + " rules Started at " + DateTime.Now.ToString("HH:mm:ss tt"));
                        Log("Round " + rulesNumber + " Started with " + fileName, w);

                        netFinder.Reset();
                        net = netFinder.FindOptimalSolution(X, Y, rulesNumber, 5);


                        TextWriter writer = new StreamWriter(((netFinder.ANFISMode) ? 'a' : 'z') + "Net r" + rulesNumber + "x" + X[0].Length + "set(" + X.Length + ") mse" + netFinder.CurrentOptimalNet.CurrentMinErr + ".xml");
                        serializer.Serialize(writer, net);
                        writer.Close();

                        Log("mse" + netFinder.CurrentOptimalNet.CurrentMinErr, w);
                        Console.WriteLine("   mse is" + netFinder.CurrentOptimalNet.CurrentMinErr);
                        Log(netFinder.CurrentOptimalNet.CurrentBestNet.ToString(), w);

                    }
                }
            }
            
        }


        /// <summary>
        /// Loads serialised networks from XML files and calculates MAPE, MAE & MSE
        /// </summary>
        /// <param name="serializer"></param>
        private static void CalculateMAPE_MAE_MSE_ThroughAllNetsAndAllDatasets(XmlSerializer serializer)
        {

            OptimalNetExpert<FastBellFunc> netFinder = new OptimalNetExpert<FastBellFunc>();
            bool ANFISMode = false;
            double[][] X;
            double[] Y;


            for (int featuresN = 2; featuresN <= 4; featuresN++)
            {
                using (StreamWriter w = File.AppendText("logMAPE_MSE.txt"))
                {
                    Console.WriteLine(" Loading dataset with " + featuresN + " features...");
                    string fileName = "..\\..\\..\\data\\Input50n x" + featuresN + ".csv";
                    LoadData(fileName, out  X, out Y);

                    TSK<FastBellFunc> net = new TSK<FastBellFunc>();
                    int startWithRulesN = 5;
                    int lastRuleN = 12;
                    double[] mses = new double[lastRuleN - startWithRulesN];
                    double[] maes = new double[lastRuleN - startWithRulesN];
                    double[] mapes = new double[lastRuleN - startWithRulesN];
                    for (int rulesNumber = startWithRulesN; rulesNumber < lastRuleN; rulesNumber++)
                    {

                        Console.WriteLine("   Loading net with " + rulesNumber + " rules Started at " + DateTime.Now.ToString("HH:mm:ss tt"));
                        Log("Round " + rulesNumber + " Started with " + fileName, w);

                        FileStream fs = new FileStream("..\\..\\..\\netsnomes\\" + ((ANFISMode) ? 'a' : 'z') + "Net r" + rulesNumber + "x" + X[0].Length + "set(" + X.Length + ").xml", FileMode.Open);
                        net = (TSK<FastBellFunc>)serializer.Deserialize(fs);
                        fs.Close();

                        mses[rulesNumber - startWithRulesN] = MSE(net, X, Y);
                        maes[rulesNumber - startWithRulesN] = MAE(net, X, Y);
                        mapes[rulesNumber - startWithRulesN] = MAPE(net, X, Y);

                    }
                    Log("start=" + startWithRulesN + " fin=" + lastRuleN + " mse=" + DoubleArrayToWolframForm(mses), w);
                    Log("start=" + startWithRulesN + " fin=" + lastRuleN + " mae=" + DoubleArrayToWolframForm(maes), w);
                    Log("start=" + startWithRulesN + " fin=" + lastRuleN + " mape=" + DoubleArrayToWolframForm(mapes), w);
                    Console.WriteLine("start=" + startWithRulesN + " mse=" + DoubleArrayToWolframForm(mses));

                }
            }
            PAKTE();
        }

        #endregion


        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }

        public static void PAKTE()
        {
            Console.Beep();
            Console.WriteLine("Press anykey to exit...");
            Console.ReadKey();
        }

        #region Criteria
        private static double MSE(TSK<FastBellFunc> net, double[][] X, double[] Y)
        {
            Trainer<TSK<FastBellFunc>> tr = new Trainer<TSK<FastBellFunc>>(net);
            return tr.MeanSqrError(X, Y);
        }

        private static double MAE(TSK<FastBellFunc> net, double[][] X, double[] Y)
        {
            Trainer<TSK<FastBellFunc>> tr = new Trainer<TSK<FastBellFunc>>(net);
            return tr.MAE(X, Y);
        }

        private static double MAPE(TSK<FastBellFunc> net, double[][] X, double[] Y)
        {
            Trainer<TSK<FastBellFunc>> tr = new Trainer<TSK<FastBellFunc>>(net);
            return tr.MAPE(X, Y);
        }
        #endregion 

        /// <summary>
        /// Function is used to export to Wolfram Mathematica
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        static string DoubleArrayToWolframForm(double[] x)
        {
            string res = "{";
            for (int i = 0; i < x.Length;i++ )
                res += (string)((i == 0) ? " " : ",") + x[i].ToString().Replace(',','.');
                return res + "}";
        }

        #region Events Raisers

        public void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        }

        public void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Console.WriteLine("Unknown attribute " +
            attr.Name + "='" + attr.Value + "'");
        }

        public static void GradLog(int rule, int conj, int param, int cnt, double h0, double prevMseValTest, double prevMseValTrain)
        {
            Console.WriteLine("ijk={0}{1}{2}   cnt={3}\th0={4}\tprevVal={5}", rule, conj, param, cnt, h0, prevMseValTest);
        }

        /// <summary>
        /// Logging for test purposes
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="MSEtrainCurrent"></param>
        /// <param name="MSEtrainPrev"></param>
        /// <param name="MSEtestCurrent"></param>
        /// <param name="MSEtestPrev"></param>
        /// <param name="h0"></param>
        /// <param name="cnt"></param>
        private static void Log(Trainer<TSK<FastBellFunc>> tr, double MSEtrainCurrent, double MSEtrainPrev, double MSEtestCurrent, double MSEtestPrev, double h0, int cnt)
        {
            if (Math.Abs(h0) <= tr.hEps)
                Console.WriteLine("Stop by h0==({0}) <= hEps", h0);
            if (cnt >= tr.MaxIterationsCount) 
                Console.WriteLine("Stop by max iter cnt==({0}) ", cnt);
            if (Math.Abs(MSEtrainCurrent - MSEtrainPrev) <= tr.dMSE_Eps)
                Console.WriteLine("Stop by  Math.Abs(MeanSqrError(X, Y)-prevVal)==({0}) <=eps", Math.Abs(MSEtrainCurrent - MSEtrainPrev));
            Console.WriteLine();
        }
        #endregion

        #region LoadData
        /// <summary>
        /// Takes CSV table and reads dataset from it. Expects the last column of the dataset to be Y.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="X"></param>
        /// <param name="Y">last column of dataset</param>
        private static void LoadData(string fileName, out double[][] X, out double[] Y)
        {
            List<List<double>> inArr = new List<List<double>>();
            inArr = ParseCSV(fileName);
            FormXY(inArr, out X, out Y);
        }

        private static void FormXY(List<List<double>> inArr, out double[][] X, out double[] Y)
        {
            int xCnt = inArr[0].Count - 1;
            X = new double[inArr.Count][];
            Y = new double[inArr.Count];

            for (int row = 0; row < inArr.Count; row++)
            {
                X[row] = new double[xCnt];
                for (int col = 0; col < inArr[row].Count; col++)
                    if (col < xCnt)
                        X[row][col] = inArr[row][col];
                    else
                        Y[row] = inArr[row][col];
            }
        }

        private static List<List<double>> ParseCSV(string fn)
        {
            List<List<double>> inArr = new List<List<double>>();
            using (CsvFileReader reader = new CsvFileReader(fn))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    List<double> sList = new List<double>();
                    foreach (string s in row)
                        sList.Add(Double.Parse(s));
                    inArr.Add(sList);
                }
            }
            return inArr;
        }


        #endregion
    }

}

