﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSKNet.TSKModule.FuzzyRules
{
    public struct FuzzyNumber
    {
        public double W, Val;
        

        public FuzzyNumber(double w, double val)
        {
            this.Val = val;
            this.W = w;
        }

        public override string ToString()
        {
            return "{"+Val+"+W("+W+")}";
        }

    }
}
