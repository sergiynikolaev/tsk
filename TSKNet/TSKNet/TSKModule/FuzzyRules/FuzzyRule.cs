﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSKNet.TSKModule.MembershipFunctions;
using TSKNet.TSKModule.FuzzyRules;
using TSKNet.TSKModule.Ranges;
using System.Xml.Serialization;

namespace TSKNet.TSKModule
{
    public class FuzzyRule<T> where T : W, ICloneable, new()
    {
        #region INITIALIZATION
        
        [XmlArrayAttribute]
        public T[] FuzzyConjuncts;

        public int ConjunctsCount { get { return FuzzyConjuncts.Length; } }

        [XmlArrayAttribute]
        public double[] Ps;

        public FuzzyRule(int ruleLength)
        {
            Ps = new double[ruleLength+1];

            FuzzyConjuncts = new T[ruleLength];
            for (int i = 0; i < FuzzyConjuncts.Length; i++)
                FuzzyConjuncts[i] = new T();
        }

        public FuzzyRule()
        {
        }


        public void Init(InputRanges varRange)
        {
            for (int i = 0; i < varRange.Length; i++)
                FuzzyConjuncts[i].Init(varRange[i]);
        }

        public void Init(int varRangeLength)
        {
            for (int i = 0; i < varRangeLength; i++)
                FuzzyConjuncts[i].Init();
        }
        #endregion
       
        #region SIMULATION
        public double Simulate(double[] x)
        {
            if (x.Length != FuzzyConjuncts.Length) throw new ArgumentException();
            double  yVal = 0;
            for (int i = 0; i < x.Length; i++)
                yVal += Ps[i + 1] * x[i];
            yVal += Ps[0];
            return  yVal;
        }

        public double CalculateFuzziness(double[] x)
        {
            double w = 1;
            for (int i = 0; i < x.Length; i++)
                w *= FuzzyConjuncts[i].Value(x[i]);
            return w;
        }
        #endregion

        #region DERIVATIVE
        public double dWRule_dpi(double[] x, int conjunctIndex,int paramIndex)
        {
            double w = 1;
            for (int i = 0; i < x.Length; i++)
                if (i != conjunctIndex)
                    w *= FuzzyConjuncts[i].Value(x[i]);
            return w * Simulate(x) * FuzzyConjuncts[conjunctIndex].dW_dpi(x[conjunctIndex], paramIndex);
        }
        #endregion

        #region PARAMETRIC P - IDENTIFICATION

        public double[] PMatrixIdentificationRowValues(double[] x)
        {
            double[] res = new double[Ps.Length ];
            double w = CalculateFuzziness(x);
            for (int i = 0; i < Ps.Length ; i++)
                res[i] = w * ((i > 0) ? x[i - 1] : 1.0);
            return res;
        }

        #endregion

        #region Training
        public void ModifyParams(double delta, int pIndex,int conjunctIndex)
        {
            FuzzyConjuncts[conjunctIndex].ModifyParams(delta, pIndex);
        }
        #endregion

        #region OutPut
        public override string ToString()
        {
            string res = "\t\t{{\n";
            if (FuzzyConjuncts.Length > 0)
                res +=  FuzzyConjuncts[0].ToString() ;
            for(int i = 1;i< FuzzyConjuncts.Length;i++)
                res +=";"+FuzzyConjuncts[i].ToString();
            return res+"};\n{"+OutputP()+"}\n\t\t}\n";
        }

        private string OutputP()
        {
            string res = " ";
            if (Ps.Length > 0)
                res += Ps[0].ToString()+";{";
            for (int i = 1; i < Ps.Length; i++)
                res += ((i == 1) ? "" : ";") + Ps[i].ToString();
            return res +"} ";
        }
        #endregion

        #region IClonable

        public FuzzyRule(FuzzyRule<T> fr)
        {
            Ps = (double[])fr.Ps.Clone();
            DeepClone(fr);
        }

        private void DeepClone(FuzzyRule<T> fr)
        {
            FuzzyConjuncts = new T[fr.FuzzyConjuncts.Length];
            for (int i = 0; i < FuzzyConjuncts.Length; i++)
                FuzzyConjuncts[i] = (T)fr.FuzzyConjuncts[i].Clone();
        }

        public object Clone()
        {
            return new FuzzyRule<T>(this);
        }
        #endregion
    }
}
