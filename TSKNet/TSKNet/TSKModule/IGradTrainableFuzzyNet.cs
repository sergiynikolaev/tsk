﻿using System;
using TSKNet.TSKModule.MembershipFunctions;
namespace TSKNet.TSKModule
{
    public interface IGradTrainableFuzzyNet
    {
        int RulesNumber { get; }
        int RuleLength { get; }
        int MembershipFunctionParamCount { get; }

        void InitTraining(double[][] x, double[] y);
        double dEs_dPijk(double[][] x, double[] y, int ruleIndexI, int conjunctIndexJ, int paramIndexK);
        void ModifyParameter(int ruleNumber, int conjNumber, double deltaValue, int paramNumber);
        TSKNet.TSKModule.FuzzyRules.FuzzyNumber Simulate(double[] x);

        void InitANFISRules();
    }
}
