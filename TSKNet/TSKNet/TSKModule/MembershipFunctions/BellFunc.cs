﻿using System;
using TSKNet.TSKModule.Ranges;

namespace TSKNet.TSKModule.MembershipFunctions
{
    public class BellFunc : W
    {
        #region INIT
        public static int pCount { get { return 3; } }
    
        public BellFunc()
        {
            wParameters = new double[pCount];
        }


        public override void Init(WRanges _r)
        {
            Ranges = _r;
            for (int i = 0; i < pCount; i++)
                wParameters[i] = Ranges.R[i].ValueInRange;
        }

        public override void Init()
        {
            Ranges = new WRanges(pCount);
            Ranges.R[0] = new Range(0, 1);
            Ranges.R[1] = new Range(0.1, 2);
            Ranges.R[2] = new Range(1, 2);
            for (int i = 0; i < pCount; i++)
                wParameters[i] = Ranges.R[i].ValueInRange;
        }
 
        #endregion
        
        #region SIMULATION
        public override double Value(double x)
        {
            if (wParameters[1] != 0)
                return 1.0 / (1.0 + Math.Pow((x - wParameters[0]) / wParameters[1], 2 * wParameters[2]));
//                return 1.0 / (1.0 + Math.Pow(Math.Pow((x - wParameters[0]) / wParameters[1], 2 ), wParameters[2]));
            else
                return 0.0;
        }
        #endregion

        #region DERIVATIVE
        public override double dW_dpi(double x, int numer)
        {
            double c = wParameters[0];
            double b = wParameters[1];
            double d = wParameters[2];
            double xc_b = (x - c) / b;
            double xc_b2 = Math.Pow(xc_b, 2);
            double xc_b2d = Math.Pow(xc_b2, d);
            double denominator = Math.Pow(1 + xc_b2d, 2);

            switch (numer)
            {
                case 0:
                    return dW_dp0(b, d, xc_b, xc_b2d, denominator);
                case 1:
                    return dW_dp1(b, d, xc_b2d, denominator);
                case 2:
                    return dW_dp2(xc_b2, xc_b2d, denominator);
                default:
                    throw new ArgumentException();
            }
        }

        /*public override double[] dW_dpis(double x)
        {
            double[] res = new double[pCount];
            
            double c = wParameters[0];
            double b = wParameters[1];
            double d = wParameters[2];
            double xc_b = (x - c) / b;
            double xc_b2 = Math.Pow(xc_b, 2);
            double xc_b2d = Math.Pow(xc_b2, d);
            double denominator = Math.Pow(1 + xc_b2d, 2);

            res[0] = dW_dp0(b, d, xc_b, xc_b2d, denominator);
            res[1] = dW_dp1(b, d, xc_b2d, denominator);
            res[2] = dW_dp2(xc_b2, xc_b2d, denominator);
            return res;
        }
        */
        private static double dW_dp0(double b, double d, double xc_b, double xc_b2d, double denominator)
        {
            return  2.0 * d * xc_b2d / (b * denominator * xc_b);
        }
        private static double dW_dp1(double b, double d, double xc_b2d, double denominator)
        {
            return 2.0 * d * xc_b2d / (b * denominator);
        }
        private static double dW_dp2( double xc_b2, double xc_b2d, double denominator)
        {
            return -Math.Log(xc_b2) * xc_b2d / denominator;
        }

        #endregion


    }
}
