﻿using System;
using TSKNet.TSKModule.Ranges;
using System.Xml.Serialization;

namespace TSKNet.TSKModule.MembershipFunctions
{
    public class FastBellFunc : W,ICloneable
    {
        #region INIT
        public const int pCount = 2; 

        public FastBellFunc()
        {
            wParameters = new double[pCount];
            Init();
        }

        public FastBellFunc(FastBellFunc fb)
        {
            wParameters = (double[])fb.wParameters.Clone();
        }

        public override void Init(WRanges _r)
        {
            Ranges = _r;
            for (int i = 0; i < ParamsCount; i++)
                wParameters[i] = Ranges.R[i].ValueInRange;
        }

        public override void Init()
        {
            Ranges = new WRanges(ParamsCount);
            Ranges.R[0] = new Range(0, 1);
            Ranges.R[1] = new Range(0.1, 2);
            for (int i = 0; i < ParamsCount; i++)
                wParameters[i] = Ranges.R[i].ValueInRange;
        }
        #endregion
        
        #region SIMULATION
        public override double Value(double x)
        {
            if (wParameters[1] != 0)
                return 1.0 / (1.0 + Math.Pow((x - wParameters[0]) / wParameters[1], 2));
            else
                return 0.0;
        }
        #endregion

        #region DERIVATIVE
        public override double dW_dpi(double x, int numer)
        {
            double b = wParameters[1];
            double c = wParameters[0];
            double b2 = Math.Pow(b, 2);
            double xc = (x - c);
            switch (numer)
            {
                case 0:
                    return 2.0 * b2 * xc / Math.Pow(b2 + Math.Pow(xc, 2), 2);
                case 1:
                    return 2.0 * b * Math.Pow(xc, 2) / Math.Pow(b2 + Math.Pow(xc, 2), 2);
                default:
                    throw new ArgumentException();
            }
        }
        #endregion

        public object Clone()
        {
            return new FastBellFunc(this);
        }
    }
}
