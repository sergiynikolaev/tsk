﻿
using TSKNet.TSKModule.Ranges;
using System;
using System.Xml.Serialization;

namespace TSKNet.TSKModule.MembershipFunctions
{
    public abstract class W
    {

        public int ParamsCount { get { return wParameters.Length; } }

        //[XmlAttribute]
        public static WRanges Ranges;

        [XmlArrayAttribute]
        public double[] wParameters;


        public W() { }
        public W(W w)
        {
            wParameters = (double[])w.wParameters.Clone();
        }

        public double this[int i]// W[3] = 5;
        {
            set { wParameters[i] = value; }
            get { return wParameters[i]; }
        }

        public abstract void Init(WRanges r);
        public abstract void Init();


        public abstract double Value(double x);

        public abstract double dW_dpi(double x, int numer);

        #region Training
        public static bool UseFitToRangeFeature = true;
        public void ModifyParams(double delta, int pIndex)
        {
            //Console.WriteLine("wPar( " + pIndex + " ) = " + (wParameters[pIndex] + delta) );
            if (UseFitToRangeFeature)
                wParameters[pIndex] = Ranges.R[pIndex].FitToRange(wParameters[pIndex] + delta);
            else
                wParameters[pIndex] = wParameters[pIndex] + delta;
        }
        #endregion

        public override string ToString()
        {
            string res = "{";
            if (wParameters.Length > 0)
                res += wParameters[0];
            for (int i = 1; i < wParameters.Length; i++)
                res += " ; " + wParameters[i];
            return res + "}";
        }

    }
}
