﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSKNet.TSKModule.MembershipFunctions;

namespace TSKNet.TSKModule.ParametricIdentification
{
    public class CurrentOptimalSolution<T> where T : ICloneable
    {

        public void Reset()
        {
            currentMinErr = Double.MaxValue;
        }

        public bool HasNetInside() { return currentMinErr != Double.MaxValue; }

        private T tskNet;
        public T CurrentBestNet { get { return tskNet; } }

        private double currentMinErr = Double.MaxValue;
        public double CurrentMinErr { get { return currentMinErr; } }


        public void TestForBest(T net, double err)
        {
            if (currentMinErr >= err)
            {
                tskNet = (T)net.Clone();
                currentMinErr = err;
            }
        }



    }
}
