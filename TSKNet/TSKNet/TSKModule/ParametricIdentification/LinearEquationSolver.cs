﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Double;

namespace TSKNet.TSKModule.ParametricIdentification
{
    public static class LinearEquationSolver
    {
        public static double[] Solve(double[,] m,double[] b)
        {
            var matrixA = new DenseMatrix(m);
            var vectorB = new DenseVector(b);
            var resultX = matrixA.LU().Solve(vectorB);
//            Console.WriteLine(matrixA.Multiply(resultX).Subtract(vectorB).Norm(2));
//            resultX = matrixA.QR().Solve(vectorB); //Slover but more precise
            return resultX.ToArray();
        }
    }
}
