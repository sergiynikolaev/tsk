﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSKNet.TSKModule.MembershipFunctions;

namespace TSKNet.TSKModule.ParametricIdentification
{
    /// <summary>
    /// This class implements metatraining strategies to fing global optimal solution
    /// Trains fuzzy networks with different init parameters to obtain general global solution.
    /// Globaly optimal solutions in sense of constant given initial conditions are obtained (with early stopping criteria) by net.Train function
    /// </summary>
    /// <typeparam name="T">fuzzy number model </typeparam>
    public class OptimalNetExpert<T> where T : W, ICloneable, new()
    {
        public OptimalNetExpert() { CurrentOptimalNet = new CurrentOptimalSolution<TSK<T>>(); }

        public CurrentOptimalSolution<TSK<T>> CurrentOptimalNet;


        internal void Reset()
        {
            CurrentOptimalNet.Reset();
        }

        /// <summary>
        /// Training parameters
        /// 
        /// </summary>
        public double H0 = 0.001;
        public double Growth = 1.1;
        public double Fade = 0.5;
        public double dMSE_Eps = 10E-25;
        public double hEps = 10E-25;
        public int MaxIterationsCount = 1000;
        public int TrainCycles = 5;
        public bool ANFISMode = true;


        /// <summary>
        /// Takes training data X with coresponding output, and returns trained net
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="rulesNumber">how many rules should be in FNN </param>
        /// <param name="InitRoundsCount">how many reinitialisations should be performed to find optimal solution (the more - the better, but slower)</param>
        /// <returns>trained model</returns>
        public TSK<T> FindOptimalSolution(double[][] X, double[] Y, int rulesNumber, int InitRoundsCount)
        {
            CurrentOptimalNet.Reset();
            TSK<T> net = new TSK<T>(rulesNumber, X[0].Length);
            for (int i = 0; i < InitRoundsCount; i++)
            {
                net = GetTrainedNet(X, Y, rulesNumber);
                RaiseRoundIsOver(net);
            }
            return net;
        }

        /// <summary>
        /// Creates and trains TSK FNN with given dataset and rules number
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="rulesNumber"></param>
        /// <returns>trained model</returns>
        private TSK<T> GetTrainedNet(double[][] X, double[] Y, int rulesNumber)
        {
            TSK<T> net = new TSK<T>(rulesNumber, X[0].Length);
            Trainer<TSK<T>> tr = new Trainer<TSK<T>>(net);
            CurrentOptimalNet.TestForBest(net,tr.MeanSqrError(X,Y));
            tr.H0 = H0;
            tr.Growth = Growth;
            tr.Fade = Fade;
            tr.dMSE_Eps = dMSE_Eps;
            tr.hEps = hEps;
            tr.MaxIterationsCount = MaxIterationsCount;
            tr.ANFISMode = ANFISMode;
            net = tr.Train(X, Y, TrainCycles);
            CurrentOptimalNet.TestForBest(net, tr.MeanSqrError(X, Y));
            return CurrentOptimalNet.CurrentBestNet;
        }


        public delegate void RoundIsOverEventHandler(TSK<T> net);
        public event RoundIsOverEventHandler OnRoundIsOverEvent;
        private void RaiseRoundIsOver(TSK<T> net)
        {
            if (OnRoundIsOverEvent != null) OnRoundIsOverEvent(net);
        }


    }
}
