﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSKNet.TSKModule.ParametricIdentification
{
    public class Trainer<T>  where T : IGradTrainableFuzzyNet, ICloneable
    {

        #region INITIALIATION
        
        private T net;
        public Trainer(T net)
        {
            this.net = net;
        }
        
        #endregion

        private CurrentOptimalSolution<T> cos = new CurrentOptimalSolution<T>();

        #region TRAININIG LOGIC

        public double Fade = 0.5;
        public double Growth = 1.1;
        public double H0 = 0.01;
        public double MaxIterationsCount = 1000;
        public double dMSE_Eps=10E-25;
        public double hEps = 10E-25;

        /// <summary>
        /// Uses deSilva-Almeida-BP 
        /// Iteratively fits each single given parameter
        /// </summary>
        /// <param name="trainX"></param>
        /// <param name="trainY"></param>
        /// <param name="rule"></param>
        /// <param name="conj"></param>
        /// <param name="param"></param>
        /// <param name="dMSE_Eps"></param>
        /// <param name="hEps"></param>
        /// <param name="MaxIterationsCount"></param>
        private void GradientLearner(double[][] trainX, double[] trainY,double[][] testX, double[] testY, int rule, int conj, int param)
        {
            double h0 = Math.Abs(H0 / net.dEs_dPijk(trainX, trainY, rule, conj, param));
            int cnt = 0;

            double prevMseValTest = Double.MaxValue;
            double nextMseValTest = MeanSqrError(testX, testY);

            double prevMseValTrain = Double.MaxValue;
            double nextMseValTrain = MeanSqrError(trainX, trainY);

            while (Math.Abs(h0) > hEps && cnt < MaxIterationsCount && Math.Abs(nextMseValTest - prevMseValTest) > dMSE_Eps)
            {
                cnt++;
                prevMseValTest = nextMseValTest;
                prevMseValTrain = nextMseValTrain;

                RaiseGradLogEvent(rule, conj, param, cnt, h0, prevMseValTest, nextMseValTrain);

                h0 = ModifyNetAndH0(trainX, trainY, rule, conj, param, h0);
                Raiseh0Event(rule, conj, param, h0);

                nextMseValTest = MeanSqrError(testX, testY);
                nextMseValTrain = MeanSqrError(trainX, trainY);
                cos.TestForBest(net, nextMseValTest);
            }
            RaiseMSETestEvent(rule, conj, param, prevMseValTest);
            RaiseMSETrainEvent(rule, conj, param, prevMseValTrain);
            RaiseIterationEndLogEvent(nextMseValTrain, prevMseValTrain, nextMseValTest, prevMseValTest, h0, cnt);
        }

        private double ModifyNetAndH0(double[][] trainX, double[] trainY, int rule, int conj, int param, double h0)
        {
            double deriv = net.dEs_dPijk(trainX, trainY, rule, conj, param);
            RaiseDerivValEvent(rule, conj, param, deriv);
            net.ModifyParameter(rule, conj, -h0 * deriv, param);
            double newDeriv = net.dEs_dPijk(trainX, trainY, rule, conj, param);
            return h0 * ((deriv * newDeriv > 0) ? Growth : Fade);
        }

        private double TrainIteration(double[][] trainX, double[] trainY, double[][] testX, double[] testY)
        {
            InitOnFirstIteration(trainX, trainY);
            cos.TestForBest(net,MeanSqrError(testX, testY));
            for (int i = 0; i < net.RulesNumber; i++)
                for (int j = 0; j < net.RuleLength; j++)
                    for (int k = 0; k < net.MembershipFunctionParamCount; k++)
                    {
                        GradientLearner(trainX, trainY, testX, testY, i, j, k);
                        net = cos.CurrentBestNet;
                    }
            return MeanSqrError(testX, testY);
        }


        public bool InitOnce = false;
        private bool firstIteration;

        public bool ANFISMode = false;
        private void InitOnFirstIteration(double[][] trainX, double[] trainY)
        {
            if (ANFISMode)
            {
                if (firstIteration)
                {
                    net.InitTraining(trainX, trainY);
                    net.InitANFISRules();
                    firstIteration = false;
                }
            }
            else
            if (!InitOnce)
                net.InitTraining(trainX, trainY);
            else
                if (firstIteration)
                {
                    net.InitTraining(trainX, trainY);
                    firstIteration = false;
                }
        }

        public T Train(double[][] trainX, double[] trainY,double[][] testX, double[] testY, int maxIterations = 3)
        {
            firstIteration = true;
            for (int i = 0; i < maxIterations; i++)
                TrainIteration(trainX, trainY, testX, testY);
            return net;
        }

        public T Train(double[][] X, double[] Y, int maxIterations = 3)
        {
            firstIteration = true;
            for (int i = 0; i < maxIterations; i++)
                TrainIteration(X, Y, X, Y);
            return net;
        }
        #endregion

        #region Criteria
        public double MeanSqrError(double[][] x, double[] y)
        {
            double eSum = 0;
            for (int i = 0; i < y.Length; i++)
                eSum += Math.Pow(net.Simulate(x[i]).Val - y[i], 2);
            return eSum / y.Length;
        }

        public double MAPE(double[][] x, double[] y)
        {
            double eSum = 0;
            for (int i = 0; i < y.Length; i++)
                eSum += Math.Abs(net.Simulate(x[i]).Val - y[i]) / ((y[i]!=0)? y[i]:1);
            return eSum / y.Length;
        }

        public double MAE(double[][] x, double[] y)
        {
            double eSum = 0;
            for (int i = 0; i < y.Length; i++)
                eSum += Math.Abs(net.Simulate(x[i]).Val - y[i]);
            return eSum / y.Length;
        }


        #endregion

        #region Logging & testing

        public delegate void ParamLogEventHandler(int rule, int conj, int param, double parameter);
        public event ParamLogEventHandler MSETestEvent;
        protected virtual void RaiseMSETestEvent(int rule, int conj, int param, double mse)
        {
            if (MSETestEvent != null) MSETestEvent(rule, conj, param, mse);
        }

        public event ParamLogEventHandler MSETrainEvent;
        protected virtual void RaiseMSETrainEvent(int rule, int conj, int param, double mse)
        {
            if (MSETrainEvent != null) MSETrainEvent(rule, conj, param,mse);
        }
        
        
        public event ParamLogEventHandler OnDerivValChangeEvent;
        protected virtual void RaiseDerivValEvent(int rule, int conj, int param, double deriv)
        {
            if (OnDerivValChangeEvent != null) OnDerivValChangeEvent(rule, conj, param,deriv);
        }
        
        public event ParamLogEventHandler h0Event;
        protected virtual void Raiseh0Event(int rule, int conj, int param, double h0)
        {
            if (h0Event != null) h0Event(rule, conj, param,h0);
        }

        public delegate void IterationEndLogEventHandler(Trainer<T> sender, double MSEtrainCurrent, double MSEtrainPrev, double MSEtestCurrent, double MSEtestPrev, double h0, int cnt);
        public event IterationEndLogEventHandler IterationEndLogEvent;
        protected virtual void RaiseIterationEndLogEvent(double MSEtrainCurrent, double MSEtrainPrev, double MSEtestCurrent, double MSEtestPrev, double h0, int cnt)
        {
            if (IterationEndLogEvent != null) IterationEndLogEvent(this, MSEtrainCurrent, MSEtrainPrev, MSEtestCurrent, MSEtestPrev, h0, cnt);
        }

        public delegate void GradLogEventHandler(int rule, int conj, int param, int cnt, double h0, double prevMseValTest, double prevMseValTrain);
        public event GradLogEventHandler GradLogEvent;
        protected virtual void RaiseGradLogEvent(int rule, int conj, int param, int cnt, double h0, double prevMseValTest , double prevMseValTrain)
        {
            if (GradLogEvent != null) GradLogEvent(rule, conj, param, cnt, h0, prevMseValTest, prevMseValTrain);
        }



        #endregion

    }
}
