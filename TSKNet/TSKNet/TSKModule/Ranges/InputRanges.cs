﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSKNet.TSKModule.Ranges
{
    public class InputRanges
    {
        private WRanges[] wrns;
        public WRanges this[int i]
        {
            get { return wrns[i]; }
            set { wrns[i] = value; }
        }

        public int Length { get { return wrns.Length; } }

        public InputRanges(int inputLength,int paramsCount)
        {
            wrns = new WRanges[inputLength];
            for (int i = 0; i < inputLength; i++)
                wrns[i] = new WRanges(paramsCount);
        }
    }
}
