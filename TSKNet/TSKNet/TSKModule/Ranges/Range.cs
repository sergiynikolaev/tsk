﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Xml.Serialization;



namespace TSKNet.TSKModule.Ranges
{
    public class Range
    {
        //[XmlAttribute]
        private double min, delta;

        public Range()
        {
            this.min = 0;
            this.delta = 1;
        }

        public Range(double min, double max)
        {
            this.min = min;
            this.delta = max - min;
        }

        private static Random r = new Random();

        public double ValueInRange
        {
            get { return min + r.NextDouble() * delta; }
        }

        public bool IsValueInRange(double v)
        {
            return (v >= min) && (v <= min + delta);
        }

        public double FitToRange(double p)
        {
            if (p < min) return min;
            else
                if (p > min + delta)
                    return min + delta;
                else return p;
        }
    }
}
