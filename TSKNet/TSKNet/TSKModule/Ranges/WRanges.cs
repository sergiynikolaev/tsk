﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSKNet.TSKModule.MembershipFunctions;
//using System.Xml.Serialization;

namespace TSKNet.TSKModule.Ranges
{
    public class WRanges
    {
       // [XmlArrayAttribute]
        public Range[] R;
       /* public Range this[int i]
        {
            get{ return Ranges[i]; }
            set{ Ranges[i]=value; }
        }*/

        public WRanges() { }

        public WRanges(int paramCnt)
        {
            R = new Range[paramCnt];
            
            R[0] = new Range(0, 1);
            R[1] = new Range(0, 1);
            
            for (int i = 2; i < paramCnt; i++)
                R[i] = new Range(1, 2);
        }

        public bool IsValueInRange(double v, int pIndex)
        {
            return R[pIndex].IsValueInRange(v);
        }
    }
}
