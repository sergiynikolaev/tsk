﻿using System;
using TSKNet.TSKModule.MembershipFunctions;
using TSKNet.TSKModule.FuzzyRules;
using TSKNet.TSKModule.Ranges;
using LinearAlgebra.Matricies;
using System.Xml.Serialization;

namespace TSKNet.TSKModule
{
    public class TSK<T> : IGradTrainableFuzzyNet, ICloneable where T : W, ICloneable, new()
    {

        #region INITIALIZATION
        [XmlArrayAttribute]
        public FuzzyRule<T>[] FuzzyRules;
        
        [XmlAttribute]
        public int RulesNumber { get { return rulesNumber; } }
        public int rulesNumber = 0;

        public int RuleLength { get { return ruleLength; } }
        public int ruleLength = 0;

        public int MembershipFunctionParamCount { get { return mfParamCount; } }
        public int mfParamCount = 0;

        public TSK(int rulesNumber, InputRanges varRange)
        {
            InitMembFunParamCount();
            ruleLength = varRange.Length;
            CreateFuzzyRules(rulesNumber, ruleLength);
            InitFuzzyRules(varRange);
        }

        private void InitMembFunParamCount()
        {
            T mf = new T();
            mfParamCount = mf.ParamsCount;
        }

        public TSK(int rulesNumber, int ruleLength)
        {
            InitMembFunParamCount();
            CreateFuzzyRules(rulesNumber, ruleLength);
            InitFuzzyRules(ruleLength);
        }

        private void CreateFuzzyRules(int rulesNumber, int ruleLength)
        {
            this.rulesNumber = rulesNumber;
            FuzzyRules = new FuzzyRule<T>[rulesNumber];
            for (int fr = 0; fr < FuzzyRules.Length; fr++)
                FuzzyRules[fr] = new FuzzyRule<T>(ruleLength);
        }

        private void InitFuzzyRules(InputRanges varRange)
        {
            for (int fr = 0; fr < FuzzyRules.Length; fr++)
                FuzzyRules[fr].Init(varRange);
        }

        private void InitFuzzyRules(int ruleLength)
        {
            this.ruleLength = ruleLength;
            for (int fr = 0; fr < FuzzyRules.Length; fr++)
                FuzzyRules[fr].Init(ruleLength);
        }


        public TSK() { }
        #endregion

        #region SIMULATION
        public FuzzyNumber Simulate(double[] x)
        {
            double w = 0,resW=0, yVal = 0;
            double ruleY,ruleW;
            for (int i = 0; i < FuzzyRules.Length; i++)
            {
                ruleY = FuzzyRules[i].Simulate(x);
                ruleW = FuzzyRules[i].CalculateFuzziness(x);
                w += ruleW;
                resW = Math.Max(resW, ruleW);
                yVal += ruleW * ruleY;
            }
            return new FuzzyNumber(resW, yVal / w);
        }

        public double CalculateFuzziness(double[] x)
        {
            double w = 0;
            for (int i = 0; i < FuzzyRules.Length; i++)
                w += FuzzyRules[i].CalculateFuzziness(x);
            return  w;
        }

        #endregion
        
        #region TRAINING
        #region PARAMETRIC P - IDENTIFICATION

        private double[] PMatrixIdentificationRowValues(double[] x)
        {
            int pLength = x.Length + 1;
            double[] res = new double[FuzzyRules.Length * pLength];
            double[] ruleRes;
            double sumAllWProduct = CalculateFuzziness(x);

            for (int i = 0; i < FuzzyRules.Length; i++)
            {
                ruleRes = FuzzyRules[i].PMatrixIdentificationRowValues(x);
                for (int j = 0; j < pLength; j++)
                    res[i * pLength + j] = ruleRes[j] / sumAllWProduct;
            }
            return res;
        }
        /// <summary>
        ///  Forms matrix W(X).P = Y
        /// </summary>
        /// <param name="x"> Each row represents one N dimensional point </param>
        /// <returns></returns>
        private double[,] PMatrixIdentificationValues(double[][] x)
        {
            int varLength = x[0].Length;
            int rowLength = FuzzyRules.Length * (varLength + 1);
            double[,] res = new double[x.Length, rowLength];
            double[] row;

            for (int i = 0; i < x.Length; i++)
            {
                row = PMatrixIdentificationRowValues(x[i]);
                for (int j = 0; j < rowLength; j++)
                    res[i, j] = row[j];
            }
            return res;
        }

        private void InsertPParametersInModel(double[] p)
        {
            for (int i = 0; i < FuzzyRules.Length; i++)
                for (int j = 0; j < FuzzyRules[i].ConjunctsCount + 1; j++)
                    FuzzyRules[i].Ps[j] = p[i * (FuzzyRules[i].ConjunctsCount + 1) + j];
        }

        public void CalculatePs(double[][] x, double[] y)
        {
            double[,] yVector = new double[y.Length, 1];
            for (int i = 0; i < y.Length; i++)
                yVector[i, 0] = y[i];
            double[] p = LeastSquaresParametersEstimation(PMatrixIdentificationValues(x), yVector);
            InsertPParametersInModel(p);
        }

        public void InitTraining(double[][] x, double[] y)
        {
            CalculatePs(x, y);
        }

        /// <summary>
        /// Ax==y
        /// </summary>
        /// <param name="A">Matrix with measurements</param>
        /// <param name="y">Outputs</param>
        /// <returns>x estimation by Mean Least Squares</returns>
        private static double[] LeastSquaresParametersEstimation(DoubleMatrix A, DoubleMatrix y)
        {
            var res = A.PseudoInverse * y;
            double[] dres = new double[res.RowCount];
            for (int i = 0; i < res.RowCount; i++)
                dres[i] = res[0, i];
            return dres;
        }
        #endregion

        #region ANFIS Mode
        public void InitANFISRules()
        {
            int n=FuzzyRules.Length;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < FuzzyRules[i].Ps.Length; j++)
                    FuzzyRules[i].Ps[j] = 0;
                FuzzyRules[i].Ps[0] = ((double)i+1.0) / ((double)n );
            }

        }
        #endregion

        #region Gradient

        private double dE_dPijk(double[] x, double y, int ruleIndexI, int conjunctIndexJ, int paramIndexK)
        {
            double totalFuzziness =CalculateFuzziness(x);
            double rulesDerivSum = FuzzyRules[ruleIndexI].dWRule_dpi(x, conjunctIndexJ, paramIndexK) * (totalFuzziness - FuzzyRules[ruleIndexI].CalculateFuzziness(x));

            return (Simulate(x).Val - y) / Math.Pow(totalFuzziness, 2) * rulesDerivSum;
        }

        public double dEs_dPijk(double[][] x, double[] y, int ruleIndexI, int conjunctIndexJ, int paramIndexK)
        {
            double eSum = 0;

            for (int i = 0; i < y.Length; i++)
                eSum += dE_dPijk(x[i],y[i],ruleIndexI, conjunctIndexJ,paramIndexK);

            return eSum ;// /  y.Length;
        }
    
        #endregion

        public void ModifyParameter(int ruleNumber, int conjNumber, double deltaValue,int paramNumber)
        {
            FuzzyRules[ruleNumber].FuzzyConjuncts[conjNumber].ModifyParams(deltaValue, paramNumber);
        }

        #endregion

        #region OutPut
        public override string ToString()
        {
            string res = "net={\n";
            if (FuzzyRules.Length > 0)
                res += FuzzyRules[0].ToString();
            for (int i = 1; i < FuzzyRules.Length; i++)
                res += ";" + FuzzyRules[i].ToString();
            return res + "\n}\n";
        }

        void OutAr(double[] a) { for (int i = 0; i < a.Length; i++) Console.Write(a[i] + " ; "); Console.WriteLine(); }
        void OutMat(double[][] a) { for (int i = 0; i < a.Length; i++) { OutAr(a[i]); Console.WriteLine(); } }
        void OutMat(double[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write(a[i, j].ToString("0.00") + " ; ");
                Console.WriteLine();
            }
        }

        #endregion

        #region ICloneable

        public TSK(TSK<T> net)
        {
            DeepFuzzyRulesClone(net);
            InitMembFunParamCount();
            rulesNumber = net.rulesNumber;
            ruleLength = net.ruleLength;
        }

        private void DeepFuzzyRulesClone(TSK<T> net)
        {
            FuzzyRules = new FuzzyRule<T>[net.FuzzyRules.Length];
            for (int i = 0; i < net.FuzzyRules.Length; i++)
                FuzzyRules[i] = (FuzzyRule<T>)net.FuzzyRules[i].Clone();
        }

        public object Clone()
        {
            return new TSK<T>(this);
        }
        #endregion

    }

 
}
