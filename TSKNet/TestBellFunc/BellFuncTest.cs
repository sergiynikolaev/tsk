﻿using TSKNet.TSKModule.MembershipFunctions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TSKNet.TSKModule.Ranges;

namespace TestBellFunc
{
    
    
    /// <summary>
    ///Это класс теста для BellFuncTest, в котором должны
    ///находиться все модульные тесты BellFuncTest
    ///</summary>
    [TestClass()]
    public class BellFuncTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///Тест для Init
        ///</summary>
        [TestMethod()]
        public void InitTest()
        {
            BellFunc target = new BellFunc(); // TODO: инициализация подходящего значения
            target.Init();
            //Assert.Inconclusive("Невозможно проверить метод, не возвращающий значение.");
        }

        /// <summary>
        ///Тест для Init
        ///</summary>
        [TestMethod()]
        public void InitTest1()
        {
            BellFunc target = new BellFunc();
            WRanges _r = new WRanges(BellFunc.pCount);
            _r.R[0] = new Range(-10, 10);
            _r.R[1] = new Range(0, 20);
            _r.R[2] = new Range(20, 30);

            target.Init(_r);
            Assert.AreEqual(BellFunc.Ranges, _r);
            //Assert.Inconclusive("Ranges");
        }

        /// <summary>
        ///Тест для Value
        ///</summary>
        [TestMethod()]
        public void ValueTest()
        {
            BellFunc target = InitBellFunction();
            double x = 0.6; // TODO: инициализация подходящего значения
            double expected = 1.0/1.0256; // TODO: инициализация подходящего значения
            double actual = target.Value(x);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Проверьте правильность этого метода теста.");
        }

        private BellFunc InitBellFunction()
        {
            BellFunc target = new BellFunc(); // TODO: инициализация подходящего значения
            target[0] = 0.5;
            target[1] = 0.25;
            target[2] = 2;
            return target;
        }

        [TestMethod()]
        public void dW_dp0Test()
        {
            BellFunc target = InitBellFunction();
            Assert.AreEqual(0.9735178798727603, target.dW_dpi(0.6, 0));
        }

        [TestMethod()]
        public void dW_dp1Test()
        {
            BellFunc target = InitBellFunction();
            Assert.AreEqual(0.38940715194910397, target.dW_dpi(0.6, 1));
        }

        [TestMethod()]
        public void dW_dp2Test()
        {
            BellFunc target = InitBellFunction();
            Assert.AreEqual(Math.Abs(0.04460127053205937-target.dW_dpi(0.6, 2))<10E-12,true);
        }
        /// <summary>
        ///Тест для pCount
        ///</summary>
        [TestMethod()]
        public void pCountTest()
        {
            int actual;
            actual = BellFunc.pCount;
            double expected = 3; 
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Проверьте правильность этого метода теста.");
        }
    }
}
