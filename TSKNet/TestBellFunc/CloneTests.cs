﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSKNet.TSKModule.MembershipFunctions;
using TSKNet.TSKModule;

namespace TestBellFunc
{
    [TestClass]
    public class CloneTests
    {
        [TestMethod]
        public void TSKTestCloneMethod()
        {
            TSK<FastBellFunc> fb1 = new TSK<FastBellFunc>(2, 3);
            TSK<FastBellFunc> fb2 = new TSK<FastBellFunc>(fb1);
            Assert.AreEqual(fb1.ToString(), fb2.ToString());
            
            fb1.ModifyParameter(0, 0, 100, 0);
            Assert.AreNotEqual(fb1.ToString(), fb2.ToString());
            
            fb2.ModifyParameter(0, 0, 100, 0);
            Assert.AreEqual(fb1.ToString(), fb2.ToString());

        }
    }
}
