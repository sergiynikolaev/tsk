﻿using TSKNet.TSKModule.MembershipFunctions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestBellFunc
{
    
    
    /// <summary>
    ///Это класс теста для FastBellFuncTest, в котором должны
    ///находиться все модульные тесты FastBellFuncTest
    ///</summary>
    [TestClass()]
    public class FastBellFuncTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        private FastBellFunc InitBellFunction()
        {
            FastBellFunc target = new FastBellFunc(); // TODO: инициализация подходящего значения
            target[0] = 0.5;
            target[1] = 0.25;
            return target;
        }

        private void AssertAreEpsEqual(double expected, double actual, double eps)
        {

            Assert.IsTrue(Math.Abs(expected - actual) < eps);
        }


        /// <summary>
        ///Тест для Value
        ///</summary>
        [TestMethod()]
        public void ValueTest()
        {
            FastBellFunc target = InitBellFunction();
            double x = 0.6; // TODO: инициализация подходящего значения
            double expected = 1.0/1.16; // TODO: инициализация подходящего значения
            double actual;
            actual = target.Value(x);
            AssertAreEpsEqual(expected, actual, 01E-12);
        }

        /// <summary>
        ///Тест для dW_dpi
        ///</summary>
        [TestMethod()]
        public void dW_dp0Test()
        {
            FastBellFunc target = InitBellFunction();
            double x = 0.6; // TODO: инициализация подходящего значения
            int numer = 0; // TODO: инициализация подходящего значения
            double expected = 2.378121284185492; // TODO: инициализация подходящего значения
            double actual;
            actual = target.dW_dpi(x, numer);
            AssertAreEpsEqual(expected, actual, 01E-12);

        }       
        /// <summary>
        ///Тест для dW_dpi
        ///</summary>
        [TestMethod()]
        public void dW_dp1Test()
        {
            FastBellFunc target = InitBellFunction();
            double x = 0.6; // TODO: инициализация подходящего значения
            int numer = 1; // TODO: инициализация подходящего значения
            double expected = 0.9512485136741966; // TODO: инициализация подходящего значения
            double actual;
            actual = target.dW_dpi(x, numer);
            AssertAreEpsEqual(expected, actual, 01E-12);

        }
    }

}
